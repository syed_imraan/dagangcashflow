import 'package:flutter/material.dart';

/*import 'planets_.dart';*/
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

class Planet {
  final String id;
  /*final String name;*/
/*  final String location;
  final String distance;
  final String gravity;*/
  final String description;
  /*final String image;*/

  const Planet({this.id, /*this.name, this.location, this.distance, this.gravity,*/
    this.description/*, this.image*/});
}

List<Planet> planets;
Planet planet;


class HomePageApi extends StatefulWidget {
  @override
  HomePageApiState createState() => new HomePageApiState();
}

class HomePageApiState extends State<HomePageApi> {

  List data;

  Future<String> getData() async {
    var response = await http.get(
    /*Uri.encodeFull("https://jsonplaceholder.typicode.com/posts"),*/
    Uri.encodeFull("https://myminiweb.com/ajax/tezt/nuubz"),
        headers: {
    "Accept": "application/json"
    }
    );

    this.setState(() {
      planet = json.decode(response.body);
      planets.add(planet);
    });
    /*print(data[1]["title"]);*/

    return "Success!";
  }

  @override
  void initState() {
    super.initState();
    this.getData();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Listviews"),
      ),
      body: new ListView.builder(
        itemCount: data == null ? 0 : data.length,
        itemBuilder: (BuildContext context, int index) {
          return new Card(
            child: new Text(data[index]["body"]),
          );
        },
      ),
    );
  }
}