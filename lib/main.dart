import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_mini_web/library/GlobalVariable.dart' as globals;

import './tabs/BankIn.dart' as _BankInTab;
import './tabs/Calendar.dart' as _CalendarPage;
import './tabs/CashIn.dart' as _CashInTab;
import './tabs/CashOut.dart' as _CashOutTab;
import './tabs/ListingPage.dart' as _ListingPage;
import './tabs/MyHomePage.dart' as _MyHomePage;
import './tabs/OneClickListingPage.dart' as _OneClickListingPage;
import './tabs/ToDoListingPage.dart' as _ToDoListingPage;
import 'menu.dart' as _menu;

void main() => runApp(new MaterialApp(
      title: 'Flutter Starter',
      theme: new ThemeData(
          primarySwatch: Colors.blueGrey,
          scaffoldBackgroundColor: Colors.white,
          primaryColor: Colors.blueGrey,
          backgroundColor: Colors.white),
      home: new Tabs(),
    ));

class FromRightToLeft<T> extends MaterialPageRoute<T> {
  FromRightToLeft({WidgetBuilder builder, RouteSettings settings})
      : super(builder: builder, settings: settings);

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    if (settings.isInitialRoute) return child;

    return new SlideTransition(
      child: new Container(
        decoration: new BoxDecoration(boxShadow: [
          new BoxShadow(
            color: Colors.black26,
            blurRadius: 25.0,
          )
        ]),
        child: child,
      ),
      position: new Tween<Offset>(
        begin: const Offset(1.0, 0.0),
        end: Offset.zero,
      ).animate(new CurvedAnimation(
        parent: animation,
        curve: Curves.fastOutSlowIn,
      )),
    );
  }

  @override
  Duration get transitionDuration => const Duration(milliseconds: 400);
}

class Tabs extends StatefulWidget {
  static String tag = 'main-page';

  @override
  TabsState createState() => new TabsState();
}

class TabsState extends State<Tabs> {
  PageController _tabController;

  var _title_app = null;
  int _tab = 0;

  @override
  void initState() {
    super.initState();
    _tabController = new PageController();
    this._title_app = TabItems[0].title;
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
        onWillPop: () => _exitApp(context),
        child: new Scaffold(
            appBar: new AppBar(
              backgroundColor: new Color(0xFFFFB300),
              title: new Text(
                _title_app,
                style: new TextStyle(
                  fontSize: Theme.of(context).platform == TargetPlatform.iOS
                      ? 17.0
                      : 20.0,
                  color: Colors.black,
                ),
              ),
              elevation:
                  Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
            ),

            //Content of tabs
            body: new PageView(
              controller: _tabController,
              onPageChanged: onTabChanged,
              children: <Widget>[
                new _OneClickListingPage.HomePage(),
                new _MyHomePage.MyHomePage(),
                new _ListingPage.HomePage()
              ],
            ),

            //Tabs
            bottomNavigationBar:
                Theme.of(context).platform == TargetPlatform.iOS
                    ? new CupertinoTabBar(
                        activeColor: Colors.blueGrey,
                        currentIndex: _tab,
                        onTap: onTap,
                        items: TabItems.map((TabItem) {
                          return new BottomNavigationBarItem(
                            title: new Text(TabItem.title),
                            icon: new Icon(TabItem.icon),
                          );
                        }).toList(),
                      )
                    : new BottomNavigationBar(
                        currentIndex: _tab,
                        onTap: onTap,
                        items: TabItems.map((TabItem) {
                          return new BottomNavigationBarItem(
                            title: new Text(TabItem.title),
                            icon: new Icon(TabItem.icon),
                          );
                        }).toList(),
                      ),

            //Drawer
            drawer: new Drawer(
                child: new ListView(
              children: <Widget>[
                new Container(
                  height: 120.0,
                  child: new DrawerHeader(
                    padding: new EdgeInsets.all(0.0),
                    decoration: new BoxDecoration(
                      color: new Color(0xFFFFB300),
                    ),
                    child: new Center(
                      child: Image.asset(
                          'assets/img/dagang_logo.png'), /*new FlutterLogo(
                      colors: Colors.blueGrey,
                      size: 54.0,
                    ),*/
                    ),
                  ),
                ),
                new ListTile(
                    leading: new Icon(Icons.arrow_downward),
                    title: new Text('Cash In'),
                    onTap: () {
                      Navigator.pop(context);
                      Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) => new _CashInTab.CashIn()),
                      );
                    }),
                new ListTile(
                    leading: new Icon(Icons.arrow_upward),
                    title: new Text('Cash Out'),
                    onTap: () {
                      Navigator.pop(context);
                      Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) => new _CashOutTab.CashOut()),
                      );
                    }),
                new Divider(),
                new ListTile(
                    leading: new Icon(Icons.attach_money),
                    title: new Text('Bank In'),
                    onTap: () {
                      Navigator.pop(context);
                      Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) => new _BankInTab.BankIn()),
                      );
                    }),
                new Divider(),
                new ListTile(
                    leading: new Icon(Icons.description),
                    title: new Text('To Do List'),
                    onTap: () {
                      Navigator.pop(context);
                      Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) =>
                                new _ToDoListingPage.HomePage()),
                      );
                    }),
                new Divider(),
                new ListTile(
                    leading: new Icon(Icons.calendar_view_day),
                    title: new Text('calendar'),
                    onTap: () {
                      Navigator.pop(context);
                      Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) =>
                                new _CalendarPage.CalendarApp()),
                      );
                    }),
                new Divider(),
                new ListTile(
                    leading: new Icon(Icons.cancel),
                    title: new Text('Sign Out'),
                    onTap: () {
                      globals.isLogin = false;
                      globals.userid = 0;
                      globals.username = "";
                      Navigator.pop(context);
                    Navigator.push(
                      context,
                      new MaterialPageRoute(builder: (context) => new _menu.MyApp()),
                    );
                    }),
              ],
            ))));
  }

  Future<bool> _exitApp(BuildContext context) {
    return showDialog(
      context: context,
      child: new AlertDialog(
        title: new Text('Do you want to Sign Out?'),
        content: new Text('Confirm?'),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: new Text('No'),
          ),
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(true),
            child: new Text('Yes'),
          ),
        ],
      ),
    ) ??
        false;
  }
  void onTap(int tab) {
    _tabController.jumpToPage(tab);
  }

  void onTabChanged(int tab) {
    setState(() {
      this._tab = tab;
    });

    switch (tab) {
      case 0:
        this._title_app = TabItems[0].title;
        break;

      case 1:
        this._title_app = TabItems[1].title;
        break;

      case 2:
        this._title_app = TabItems[2].title;
        break;
    }
  }
}

class TabItem {
  const TabItem({this.title, this.icon});

  final String title;
  final IconData icon;
}

const List<TabItem> TabItems = const <TabItem>[
  const TabItem(title: 'One Click List', icon: Icons.arrow_upward),
  const TabItem(title: 'Cash Flow', icon: Icons.description),
  const TabItem(title: 'All Records', icon: Icons.home)
];
