import 'dart:async';
import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:my_mini_web/library/GlobalVariable.dart' as globals;
import 'package:my_mini_web/tabs/ToDoListing.dart' as _ToDoTab;

String Copy = "";
String status = "";

void main() {
  runApp(new MaterialApp(
    home: new HomePage(),
  ));
}

class HomePage extends StatefulWidget {
  @override
  HomePageState createState() => new HomePageState();
}

class CustomToolTip extends StatelessWidget {
  final String text;

  CustomToolTip({this.text});

  @override
  Widget build(BuildContext context) {
    return new GestureDetector(
      child: new Tooltip(
          preferBelow: false, message: "Copy", child: new Text(text)),
      onTap: () {
        Clipboard.setData(new ClipboardData(text: text));
      },
    );
  }
}

class HomePageState extends State<HomePage> {
  List data;

  Future<String> getData() async {
    var response = await http.get(
        Uri.encodeFull(
            "http://dagang.myminiweb.com/public/api/get-all-copy/T/" +
                globals.userid.toString()),
        headers: {"Accept": "application/json"});

    this.setState(() {
      data = jsonDecode(response.body);
    });

    return "Success!";
  }

  Future<String> dltData(int id) async {
    print(id.toString());
    try {
      var connectivityResult = await (new Connectivity().checkConnectivity());
      if (connectivityResult == ConnectivityResult.mobile ||
          connectivityResult == ConnectivityResult.wifi) {
        // I am connected to a mobile network. or I am connected to a wifi network.
        status = "connected";

        var uri =
            Uri.parse("http://dagang.myminiweb.com/public/api/delete-copy");
        var request = new http.MultipartRequest("POST", uri);
        request.fields['id'] = id.toString();
        request.send().then((streamedResponse) {
          return http.Response.fromStream(streamedResponse);
        }).then((response) {
/*          print(response.body);
          _showAlert(context);*/
          var data = jsonDecode(response.body);
          if (data != null) {
            if (data["status_code"] == 200) {
              print(data["message"]);
            } else {
              print(data["message"]);
            }
          }
        });
      } else {
        status = "Not connected";
      }
      print(status);
      return status;
    } catch (e) {
      status =
          "Please Contact or Report to system Administrator"; //e.toString();
    }

    return "Success!";
  }

  /*void _showAlert(context, String msj) {
    AlertDialog dialog = new AlertDialog(
      content: new Text(msj),
      actions: <Widget>[
        new FlatButton(
          onPressed: () {
            Navigator.pop(context);
            Navigator.pop(context);
          },
          child: new Text('Ok'),
        )
      ],
    );

    showDialog(context: context, child: dialog);
  }*/

  @override
  void initState() {
    super.initState();
    this.reset();
    this.getData();
  }

  void reset() {
    globals.OneClickText = "";
  }

  void test(int index) {
    globals.OneClickText = data[index]["description"].toString();
  }

  @override
  Widget build(BuildContext context) {
    final key = new GlobalKey<ScaffoldState>();
    return new Scaffold(
      //App Bar
      appBar: new AppBar(
        backgroundColor: new Color(0xFFFFB300),
        title: new Text(
          'To Do',
          style: new TextStyle(
            fontSize:
                Theme.of(context).platform == TargetPlatform.iOS ? 17.0 : 20.0,
            color: Colors.black,
          ),
        ),
        elevation: Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
      ),
      key: key,
      body: new ListView.builder(
          itemCount: data == null ? 0 : data.length,
          itemBuilder: (BuildContext context, int index) {
            final item = data[index]['id'].toString();

            return Dismissible(
              // Each Dismissible must contain a Key. Keys allow Flutter to
              // uniquely identify Widgets.
              key: Key(item),
              // We also need to provide a function that will tell our app
              // what to do after an item has been swiped away.
              onDismissed: (direction) {
                String desc = data[index]['description'];
                dltData(data[index]['id']);
/*                String Result = _showAlert('remove? $desc', index);
                if(Result == 'Success') {*/
                Scaffold.of(context).showSnackBar(
                    SnackBar(content: Text("$desc has been removed!! ")));
                /*              }
                else  {
                  Scaffold
                      .of(context)
                      .showSnackBar(SnackBar(content: Text("Data is not Successfully deleted")));
                }*/
              },
              // Show a red background as the item is swiped away
              background: Container(color: Colors.red),
              child: new GestureDetector(
                //You need to make my child interactive
                onTap: () {
                  reset();
                  test(index);
                  Navigator.of(context).push(
                      new MaterialPageRoute(builder: (BuildContext context) {
                    return new _ToDoTab.ToDo();
                  }));
                },
                //child: body,
                child: new Card(
                  //I am the clickable child
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      new Row(
                        children: <Widget>[
                          new Expanded(
                            flex: 1,
                            child: new Padding(
                                padding: new EdgeInsets.all(30.0),
                                child: new Container(
                                  child: new Text(
                                    data[index]["description"].toString(),
                                    style: new TextStyle(fontSize: 15.0),
                                  ),
                                )),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            );
          }),
      floatingActionButton: new FloatingActionButton(
        child: new Icon(Icons.add),
        onPressed: () {
          reset();
          Navigator
              .of(context)
              .push(new MaterialPageRoute(builder: (BuildContext context) {
            return new _ToDoTab.ToDo();
          }));
        },
      ),
    );
  }
}
