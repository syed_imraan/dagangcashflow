import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_calendar/flutter_calendar.dart';
import 'package:http/http.dart' as http;
import 'package:my_mini_web/library/GlobalVariable.dart' as globals;
import 'package:my_mini_web/tabs/CalendarPage.dart' as _CalendarPage;

void main() {
  runApp(new MaterialApp(
    home: new CalendarApp(),
  ));
}

class CalendarApp extends StatefulWidget {
  @override
  CalendarViewApp createState() => new CalendarViewApp();
}

class CalendarViewApp extends State<CalendarApp> {
  List data;
  DateTime _date;

  Future<String> getData(DateTime date) async {
    print(date);
    _date = date;
    int Year = date.year;
    int Month = date.month;
    int Day = date.day;

    var response = await http.get(
        Uri.encodeFull(
            "http://dagang.myminiweb.com/public/api/get-all-copy/C/1/$Year/$Month/$Day"),
        headers: {"Accept": "application/json"});

    print(response.body);
    print(globals.userid.toString());
    this.setState(() {
      data = jsonDecode(response.body);
    });
    print(data);

    return "Success";
  }

  @override
  void initState() {
    super.initState();
  }

  void handleNewDate(date) {
    print(date);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      //App Bar
      appBar: new AppBar(
        backgroundColor: new Color(0xFFFFB300),
        title: new Text(
          'Calendar',
          style: new TextStyle(
            fontSize:
                Theme.of(context).platform == TargetPlatform.iOS ? 17.0 : 20.0,
            color: Colors.black,
          ),
        ),
        elevation: Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
      ),
      body: new ListView(
        shrinkWrap: true,
        children: <Widget>[
          new Calendar(
            onSelectedRangeChange: (range) => getData(range.item1),
            isExpandable: true,
            dayBuilder: (BuildContext context, DateTime day) {
              return new InkWell(
                onTap: () => getData(day), //print(day),
                child: new Container(
                  decoration: new BoxDecoration(
                      border: new Border.all(color: Colors.black38)),
                  child: new Text(
                    day.day.toString(),
                  ),
                ),
              );
            },
          ),
          new Divider(
            height: 5.0,
          ),
          new Text('Appointment'),
          new Divider(
            height: 5.0,
          ),
          new ListView.builder(
              itemCount: data == null ? 0 : data.length,
              shrinkWrap: true,
              itemBuilder: (BuildContext context, int index) {
//final item = data[index]['id'].toString();
                return new Card(
//I am the clickable child
                    child: new Row(
                  children: <Widget>[
                    new Expanded(
                      flex: 1,
                      child: new Padding(
                          padding: new EdgeInsets.all(5.0),
                          child: new Container(
                            child: new Text(
                              (index + 1).toString() +
                                  ".    " +
                                  data[index]["description"].toString(),
                              style: new TextStyle(fontSize: 15.0),
                            ),
                          )),
                    ),
                  ],
                ));
              }),
        ],
      ),
      floatingActionButton: new FloatingActionButton(
        child: new Icon(Icons.add),
        onPressed: () {
          Navigator
              .of(context)
              .push(new MaterialPageRoute(builder: (BuildContext context) {
            return new _CalendarPage.CalendarPage(date: _date);
          }));
        },
      ),
    );
  }
}
