import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:my_mini_web/library/GlobalVariable.dart' as globals;

final String url = "http://dagang.myminiweb.com/public/api/post-copy";
final textController = TextEditingController();
String status = "";

class ToDo extends StatefulWidget {
  @override
  _ToDoPageState createState() => new _ToDoPageState();
}

class _ToDoPageState extends State<ToDo> {
  void reset() {
    textController.text = globals.OneClickText;
  }

  @override
  void initState() {
    super.initState();
    this.reset();
  }

  postData(context) async {
    status = "";
    try {
      var connectivityResult = await (new Connectivity().checkConnectivity());
      if (connectivityResult == ConnectivityResult.mobile ||
          connectivityResult == ConnectivityResult.wifi) {
        // I am connected to a mobile network. or I am connected to a wifi network.
        status = "connected";

        var uri = Uri.parse(url);
        var request = new http.MultipartRequest("POST", uri);
        request.fields['user_id'] = globals.userid.toString();
        request.fields['description'] = textController.text;
        request.fields['type'] = 'T';
        request.send().then((streamedResponse) {
          return http.Response.fromStream(streamedResponse);
        }).then((response) {
/*          print(response.body);
          _showAlert(context);*/
          var Data = jsonDecode(response.body);
          if (Data != null) {
            if (Data["status_code"] == 200) {
              print(Data["message"]);
              _showAlert(context, Data["message"]);
            } else {
              print(Data["message"]);
              _showAlert(context, Data["message"]);
            }
          }
        });
      } else {
        status = "Not connected";
      }
      print(status);
      return status;
    } catch (e) {
      status =
      "Please Contact or Report to system Administrator"; //e.toString();
    }

    return "Success";
  }

  void _showAlert(context, String msj) {
    AlertDialog dialog = new AlertDialog(
      content: new Text(msj),
      actions: <Widget>[
        new FlatButton(
          onPressed: () {
            Navigator.pop(context);
            Navigator.pop(context);
          },
          child: new Text('Ok'),
        )
      ],
    );

    showDialog(context: context, child: dialog);
  }

  @override
  Widget build(BuildContext context) {
    final SubmitButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: Material(
        borderRadius: BorderRadius.circular(30.0),
        shadowColor: Colors.redAccent.shade100,
        elevation: 5.0,
        child: MaterialButton(
          minWidth: 200.0,
          height: 42.0,
          onPressed: () {
            //_showAlert(context);
            postData(context);
            //_showAlert(context);
          },
          //postData,
          color: Colors.black,
          child: Text('Submit', style: TextStyle(color: Colors.white)),
        ),
      ),
    );

    return Scaffold(
      //App Bar
      appBar: new AppBar(
        backgroundColor: new Color(0xFFFFB300),
        title: new Text(
          'To Do List',
          style: new TextStyle(
            fontSize:
            Theme.of(context).platform == TargetPlatform.iOS ? 17.0 : 20.0,
            color: Colors.black,
          ),
        ),
        elevation: Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
      ),
      backgroundColor: Colors.white,
      body: Material(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left: 24.0, right: 24.0, top: 24.0),
          children: <Widget>[
            new TextFormField(
              controller: textController,
              keyboardType: TextInputType.text,
              maxLines: 20,
              autofocus: false,
              decoration: InputDecoration(
                hintText: 'Anything',
                contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(32.0)),
              ),
            ),
            SubmitButton
          ],
        ),
      ),
    );
  }
}
