import 'dart:async';
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart';
import 'package:my_mini_web/library/GlobalVariable.dart' as globals;
import 'dart:convert';

final String url = "http://dagang.myminiweb.com/public/api/cash-input";
final titleController = TextEditingController();
final amountController = TextEditingController();

File g_image = null;
DateTime _date = new DateTime.now();
String status = "";

final title = TextFormField(
  controller: titleController,
  keyboardType: TextInputType.text,
  autofocus: false,
  decoration: InputDecoration(
    hintText: 'Title',
    contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
    border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
  ),
);

final amount = TextFormField(
  controller: amountController,
  keyboardType: TextInputType.number,
  autofocus: false,
  decoration: InputDecoration(
    hintText: 'Amount',
    contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
    border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
  ),
);

class CashIn extends StatefulWidget {
  @override
  _CashInPageState createState() => new _CashInPageState();
}

class _CashInPageState extends State<CashIn> {
  File _image;

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    setState(() {
      _image = image;
      g_image = image;
    });
  }

  postData(context) async {
    status = "";
    try {
      var connectivityResult = await (new Connectivity().checkConnectivity());
      if (connectivityResult == ConnectivityResult.mobile ||
          connectivityResult == ConnectivityResult.wifi) {
        // I am connected to a mobile network. or I am connected to a wifi network.
        status = "connected";

        var uri = Uri.parse(url);
        var request = new http.MultipartRequest("POST", uri);
        request.fields['title'] = titleController.text;
        request.fields['type'] = 'I';
        request.fields['amount'] = amountController.text;
        request.fields['contact_us'] = '';
        request.fields['date'] = _date.toString();
        request.fields['user_id'] = globals.userid.toString();
        request.fields['username'] = globals.username;
        var stream = new http.ByteStream(g_image.openRead());
        var length = await g_image.length();
        var multipartFile = new http.MultipartFile('file', stream, length,
            filename: basename(g_image.path));

        request.files.add(multipartFile);
        request.send().then((streamedResponse) {
          return http.Response.fromStream(streamedResponse);
        }).then((response) {
/*          print(response.body);
          _showAlert(context);*/
          var Data = jsonDecode(response.body);
          if (Data != null) {
            if (Data["status_code"] == 200) {
              print(Data["message"]);
              _showAlert(context, Data["message"]);
              //
            } else {
              print(Data["message"]);
              _showAlert(context, Data["message"]);
            }
          }
        });
      } else {
        status = "Not connected";
      }
      print(status);
      return status;
    } catch (e) {
      status =
      "Please Contact or Report to system Administrator"; //e.toString();
    }

    return "Success";
  }

  Future<Null> _SelectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: _date,
        firstDate: new DateTime(2017),
        lastDate: new DateTime(2020));

    if (picked != null && picked != _date) {
      setState(() {
        _date = picked;
      });
    }
  }

  void _showAlert(context, String msj) {
    AlertDialog dialog = new AlertDialog(
      content: new Text(msj),
      actions: <Widget>[
        new FlatButton(
          onPressed: () {
            Navigator.pop(context);
            Navigator.pop(context);
          },
          child: new Text('Ok'),
        )
      ],
    );

    showDialog(context: context, child: dialog);
  }

  @override
  Widget build(BuildContext context) {
    Widget _buildChild() {
      if (_image == null) {
        final imageText = FlatButton(
          child:
          //new Image.asset('assets/img/camera_icon.png', scale: 5.0),
          Icon(Icons.camera_alt, size: 150.0),
          onPressed: getImage,
        );
        return imageText;
      }
      final imageView = Image.file(_image);
      return imageView;
    }

    final SubmitButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: Material(
        borderRadius: BorderRadius.circular(30.0),
        shadowColor: Colors.redAccent.shade100,
        elevation: 5.0,
        child: MaterialButton(
          minWidth: 200.0,
          height: 42.0,
          onPressed: (){
            //_showAlert(context);
            postData(context);
            //_showAlert(context);
          },//postData,
          color: Colors.black,
          child: Text('Submit', style: TextStyle(color: Colors.white)),
        ),
      ),
    );

    return Scaffold(
      //App Bar
      appBar: new AppBar(
        backgroundColor: new Color(0xFFFFB300),
        title: new Text(
          'Cash In',
          style: new TextStyle(
            fontSize:
            Theme.of(context).platform == TargetPlatform.iOS ? 17.0 : 20.0,
            color: Colors.black,
          ),
        ),
        elevation: Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
      ),
      backgroundColor: Colors.white,
            body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left: 24.0, right: 24.0),
          children: <Widget>[
            SizedBox(height: 10.0),
            _buildChild(),
            SizedBox(height: 3.0),
            new Text("Snap your Receipt..",textAlign: TextAlign.center,),
            SizedBox(height: 20.0),
            title,
            SizedBox(height: 5.0),
            amount,
            //new Text('Date :  ${_date.day.toString()}/${_date.month.toString()}/${_date.year.toString()}'),
            SizedBox(height: 5.0),
            new Container(
              decoration: new BoxDecoration(
                  border: Border.all(color: Colors.black54),
                  shape: BoxShape.rectangle,
                  borderRadius: new BorderRadius.circular(20.0)),
              padding: new EdgeInsets.only(left: 20.0),
              child: new Row(
                children: <Widget>[
                  new Expanded(
                    flex: 4,
                    child: new Text(
                        'Date :  ${_date.day.toString()}/${_date.month
                            .toString()}/${_date.year.toString()}'),
                  ),
                  new Expanded(
                    child: new Container(
                      height: 40.0,
                      width: 10.0,
                      padding: EdgeInsets.only(right: 0.0),
                      decoration: new BoxDecoration(
                          color: Colors.amber,
                          shape: BoxShape.rectangle,
                          borderRadius: new BorderRadius.circular(20.0)),
                      child: new IconButton(
                          icon: new Icon(Icons.calendar_today),
                          color: Colors.white,
                          onPressed: () {
                            _SelectDate(context);
                          }),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: 11.0),
            SubmitButton
          ],
        ),
      ),
    );
  }
}
