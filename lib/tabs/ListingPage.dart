import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:my_mini_web/library/GlobalVariable.dart' as globals;
import 'package:my_mini_web/tabs/Details.dart' as _DetailsTab;
import 'package:unicorndial/unicorndial.dart';

String filterBy = "";
final key = new GlobalKey<ScaffoldState>();

void main() {
  runApp(new MaterialApp(
    home: new HomePage(),
  ));
  filterBy = "";
}

class HomePage extends StatefulWidget {
  @override
  HomePageState createState() => new HomePageState();
}

class HomePageState extends State<HomePage> {
  List dataMonthly = new List();
  List dataYearly = new List();

  Future<String> getData() async {
    if (filterBy == "") {
      var responseMonthly = await http.get(
          Uri.encodeFull(
              "http://dagang.myminiweb.com/public/api/get-all-record/monthly/" +
                  globals.userid.toString()),
          headers: {"Accept": "application/json"});
      this.setState(() {
        dataMonthly = jsonDecode(responseMonthly.body);
      });

      var responseYearly = await http.get(
          Uri.encodeFull(
              "http://dagang.myminiweb.com/public/api/get-all-record/yearly/" +
                  globals.userid.toString()),
          headers: {"Accept": "application/json"});

      this.setState(() {
        dataYearly = jsonDecode(responseYearly.body);
      });
    } else {
      var responseMonthly = await http.get(
          Uri.encodeFull(
              "http://dagang.myminiweb.com/public/api/get-all-record-by-type/monthly/" +
                  globals.userid.toString() +
                  "/" +
                  filterBy),
          headers: {"Accept": "application/json"});

      this.setState(() {
        dataMonthly = jsonDecode(responseMonthly.body);
      });

      var responseYearly = await http.get(
          Uri.encodeFull(
              "http://dagang.myminiweb.com/public/api/get-all-record-by-type/yearly/" +
                  globals.userid.toString() +
                  "/" +
                  filterBy),
          headers: {"Accept": "application/json"});

      print("yearly filter type");

      this.setState(() {
        dataYearly = jsonDecode(responseYearly.body);
      });

      filterBy = "";
    }

    return "Success!";
  }

  @override
  void initState() {
    super.initState();
    this.reset();
    this.getData();
  }

  void reset() {
    globals.title = "";
    globals.amount = "0.00";
    globals.contactus = "";
    globals.date = "";
    globals.image = "";
    globals.type = "";
  }

  @override
  Widget build(BuildContext context) {
    var childButtons = List<UnicornButton>();

    childButtons.add(UnicornButton(
        hasLabel: true,
        labelText: "Cash Out",
        currentButton: FloatingActionButton(
          onPressed: () {
            filterBy = "O";
            getData();
          },
          heroTag: "CashOut",
          backgroundColor: Colors.red,
          mini: true,
          child: Icon(Icons.attach_money),
        )));

    childButtons.add(UnicornButton(
        hasLabel: true,
        labelText: "Cash In",
        currentButton: FloatingActionButton(
            onPressed: () {
              filterBy = "I";
              getData();
            },
            heroTag: "CashIn",
            backgroundColor: Colors.green,
            mini: true,
            child: Icon(Icons.attach_money))));

    childButtons.add(UnicornButton(
        hasLabel: true,
        labelText: "Bank In",
        currentButton: FloatingActionButton(
            onPressed: () {
              filterBy = "B";
              getData();
            },
            heroTag: "BankIn",
            backgroundColor: Colors.blue,
            mini: true,
            child: Icon(Icons.credit_card, color: Colors.black26))));

    childButtons.add(UnicornButton(
        hasLabel: true,
        labelText: "All",
        currentButton: FloatingActionButton(
            onPressed: () {
              filterBy = "";
              getData();
            },
            heroTag: "All",
            backgroundColor: Colors.black,
            mini: true,
            child: Icon(Icons.clear_all, color: Colors.white))));

    return new MaterialApp(
        home: new DefaultTabController(
            length: 2,
            child: new Scaffold(
                appBar: new AppBar(
                  flexibleSpace: new Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      new TabBar(
                        tabs: [
                          new Tab(
                            text: "This Month",
                          ),
                          new Tab(
                            text: "Yearly",
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                key: key,
                floatingActionButton: UnicornDialer(
                    backgroundColor: Color.fromRGBO(255, 255, 255, 0.6),
                    parentButtonBackground: Colors.black,
                    orientation: UnicornOrientation.HORIZONTAL,
                    parentButton: Icon(Icons.apps),
                    childButtons: childButtons),
                body: TabBarView(children: <Widget>[
                  new ListView.builder(
                      itemCount: dataMonthly == null ? 0 : dataMonthly.length,
                      itemBuilder: (BuildContext context, int index) {
                        return new GestureDetector(
                          //You need to make my child interactive
                          onTap: () {
                            reset();
                            Navigator.of(context).push(new MaterialPageRoute(
                                builder: (BuildContext context) {
                              return new _DetailsTab.Details();
                            }));
                          },
                          //child: body,
                          child: new Card(
                              //I am the clickable child
                              //color: Colors.grey,
                              margin: new EdgeInsets.all(4.0),
                              child: new Padding(
                                padding: EdgeInsets.all(0.0),
                                child: new Row(
                                  children: <Widget>[
                                    new Expanded(
                                        flex: 1,
                                        child: new Padding(
                                          padding:
                                              new EdgeInsets.only(left: 0.0),
                                          child: new Row(
                                            children: <Widget>[
                                              new Container(
                                                child: Icon(
                                                    dataMonthly[index]['type']
                                                                .toString() ==
                                                            'B'
                                                        ? Icons.credit_card
                                                        : Icons.attach_money,
                                                    size: 45.0,
                                                    color: Colors.black26),
                                                color: dataMonthly[index]
                                                                ['type']
                                                            .toString() ==
                                                        'O'
                                                    ? Colors.red
                                                    : dataMonthly[index]['type']
                                                                .toString() ==
                                                            'B'
                                                        ? Colors.blue
                                                        : Colors.lightGreen,
                                                height: 60.0,
                                                width: 60.0,
                                              ),
                                              new Flexible(
                                                child: new Container(
                                                  padding: new EdgeInsets.only(
                                                      left: 8.0),
                                                  child: new Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      new Text(
                                                        dataMonthly[index]
                                                                ['title']
                                                            .toString(),
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        style: new TextStyle(
                                                          fontSize: 15.0,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                      ),
                                                      new Text(
                                                        dataMonthly[index]
                                                                ['date']
                                                            .toString(),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        )),
                                    new Expanded(
                                      flex: 0,
                                      child: new Padding(
                                        padding:
                                            new EdgeInsets.only(right: 5.0),
                                        child: new Text(
                                            'RM ' +
                                                dataMonthly[index]['amount'],
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 20.0,
                                                color: Colors.black54)),
                                      ), //new Icon(data[index]['type'] == 'B' ? Icons.credit_card : Icons.attach_money, color: Colors.black26, size: 45.0,)
                                    ),
                                    new Expanded(
                                      flex: 0,
                                      child: new Container(
                                        //color: Colors.black,
                                        width: 10.0,
//                                        decoration: new BoxDecoration(
//                                            color: Colors.amber,
//                                            shape: BoxShape.rectangle,
//                                            //borderRadius: new BorderRadius.circular(20.0)
//                                        ),
                                        padding: new EdgeInsets.only(top: 60.0),
                                        color: dataMonthly[index]['type']
                                                    .toString() ==
                                                'O'
                                            ? Colors.red
                                            : dataMonthly[index]['type']
                                                        .toString() ==
                                                    'B'
                                                ? Colors.blue
                                                : Colors.lightGreen,
                                        //child: new Text('', style: new TextStyle(color: Colors.amber),),
                                      ),
                                    )
                                  ],
                                ),
                              )),
                        );
                      }),
                  new ListView.builder(
                      itemCount: dataYearly == null ? 0 : dataYearly.length,
                      itemBuilder: (BuildContext context, int index) {
                        return new GestureDetector(
                          //You need to make my child interactive
                          onTap: () {
                            reset();
                            Navigator.of(context).push(new MaterialPageRoute(
                                builder: (BuildContext context) {
                              return new _DetailsTab.Details();
                            }));
                          },
                          //child: body,
                          child: new Card(
                              //I am the clickable child
                              //color: Colors.grey,
                              margin: new EdgeInsets.all(4.0),
                              child: new Padding(
                                padding: EdgeInsets.all(0.0),
                                child: new Row(
                                  children: <Widget>[
                                    new Expanded(
                                        flex: 1,
                                        child: new Padding(
                                          padding:
                                              new EdgeInsets.only(left: 0.0),
                                          child: new Row(
                                            children: <Widget>[
                                              new Container(
                                                child: Icon(
                                                    dataYearly[index]['type']
                                                                .toString() ==
                                                            'B'
                                                        ? Icons.credit_card
                                                        : Icons.attach_money,
                                                    size: 45.0,
                                                    color: Colors.black26),
                                                color: dataYearly[index]['type']
                                                            .toString() ==
                                                        'O'
                                                    ? Colors.red
                                                    : dataYearly[index]['type']
                                                                .toString() ==
                                                            'B'
                                                        ? Colors.blue
                                                        : Colors.lightGreen,
                                                height: 60.0,
                                                width: 60.0,
                                              ),
                                              new Flexible(
                                                child: new Container(
                                                  padding: new EdgeInsets.only(
                                                      left: 8.0),
                                                  child: new Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      new Text(
                                                        dataYearly[index]
                                                                ['title']
                                                            .toString(),
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        style: new TextStyle(
                                                          fontSize: 15.0,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                      ),
                                                      new Text(
                                                        dataYearly[index]
                                                                ['date']
                                                            .toString(),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        )),
                                    new Expanded(
                                      flex: 0,
                                      child: new Padding(
                                        padding:
                                            new EdgeInsets.only(right: 5.0),
                                        child: new Text(
                                            'RM ' + dataYearly[index]['amount'],
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 20.0,
                                                color: Colors.black54)),
                                      ), //new Icon(data[index]['type'] == 'B' ? Icons.credit_card : Icons.attach_money, color: Colors.black26, size: 45.0,)
                                    ),
                                    new Expanded(
                                      flex: 0,
                                      child: new Container(
                                        //color: Colors.black,
                                        width: 10.0,
//                                        decoration: new BoxDecoration(
//                                            color: Colors.amber,
//                                            shape: BoxShape.rectangle,
//                                            //borderRadius: new BorderRadius.circular(20.0)
//                                        ),
                                        padding: new EdgeInsets.only(top: 60.0),
                                        color: dataYearly[index]['type']
                                                    .toString() ==
                                                'O'
                                            ? Colors.red
                                            : dataYearly[index]['type']
                                                        .toString() ==
                                                    'B'
                                                ? Colors.blue
                                                : Colors.lightGreen,
                                        //child: new Text('', style: new TextStyle(color: Colors.amber),),
                                      ),
                                    )
                                  ],
                                ),
                              )),
                        );
                      }),
                ]))));
  }
}
