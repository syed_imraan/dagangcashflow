import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:my_mini_web/library/GlobalVariable.dart' as globals;
import 'package:my_mini_web/tabs/OneClick.dart' as _OneClickTab;

String Copy = "";

void main() {
  runApp(new MaterialApp(
    home: new HomePage(),
  ));
}

class HomePage extends StatefulWidget {
  @override
  HomePageState createState() => new HomePageState();
}

class CustomToolTip extends StatelessWidget {
  final String text;

  CustomToolTip({this.text});

  @override
  Widget build(BuildContext context) {
    return new GestureDetector(
      child: new Tooltip(
          preferBelow: false, message: "Copy", child: new Text(text)),
      onTap: () {
        Clipboard.setData(new ClipboardData(text: text));
      },
    );
  }
}

class HomePageState extends State<HomePage> {
  List data;

  Future<String> getData() async {
    var response = await http.get(
        Uri.encodeFull(
            "http://dagang.myminiweb.com/public/api/get-all-copy/O/" +
                globals.userid.toString()),
        headers: {"Accept": "application/json"});

    this.setState(() {
      data = jsonDecode(response.body);
    });

    return "Success!";
  }

  @override
  void initState() {
    super.initState();
    this.reset();
    this.getData();
  }

  void reset() {
    globals.OneClickText = "";
  }

  void test(int index) {
    globals.OneClickText = data[index]["description"].toString();
  }

/*
  String _showAlert(String value, int index) {
    data.removeAt(index);

    String Result = "";

    AlertDialog dialog = new AlertDialog(
      content: new Text(value, style: new TextStyle(fontSize: 30.0),),
      actions: <Widget>[
        new FlatButton(onPressed: () {Result = "Success";*/ /*_dialogResult('Yes');*/ /*}, child: new Text('Yes')),
        new FlatButton(onPressed: () {Result = "Data is not Deleted";*/ /*_dialogResult('No');*/ /*}, child: new Text('No')),
      ],
    );

    showDialog(context: context, child: dialog);
    Navigator.pop(context);
    return 'Success';
  }*/

  @override
  Widget build(BuildContext context) {
    final key = new GlobalKey<ScaffoldState>();
    return new Scaffold(
      key: key,
      body: new ListView.builder(
          itemCount: data == null ? 0 : data.length,
          itemBuilder: (BuildContext context, int index) {
            final item = data[index]['id'].toString();

            return Dismissible(
              // Each Dismissible must contain a Key. Keys allow Flutter to
              // uniquely identify Widgets.
              key: Key(item),
              // We also need to provide a function that will tell our app
              // what to do after an item has been swiped away.
              onDismissed: (direction) {
                String desc = data[index]['description'];
/*                String Result = _showAlert('remove? $desc', index);

                if(Result == 'Success') {*/
                Scaffold.of(context).showSnackBar(
                    SnackBar(content: Text("$desc has been removed!! ")));
                /*              }
                else  {
                  Scaffold
                      .of(context)
                      .showSnackBar(SnackBar(content: Text("Data is not Successfully deleted")));
                }*/
              },
              // Show a red background as the item is swiped away
              background: Container(color: Colors.red),
              child: new GestureDetector(
                //You need to make my child interactive
                onTap: () {
                  Clipboard.setData(new ClipboardData(
                      text: data[index]["description"].toString()));
                  key.currentState.showSnackBar(new SnackBar(
                    content: new Text("Copied to Clipboard"),
                  ));
                },
                onLongPress: () {
                  reset();
                  test(index);
                  Navigator.of(context).push(
                      new MaterialPageRoute(builder: (BuildContext context) {
                    return new _OneClickTab.OneClick();
                  }));
                },
                //child: body,
                child: new Card(
                  //I am the clickable child
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      new Row(
                        children: <Widget>[
                          new Expanded(
                            flex: 1,
                            child: new Padding(
                                padding: new EdgeInsets.all(30.0),
                                child: new Container(
                                  child: new Text(
                                    data[index]["description"].toString(),
                                    style: new TextStyle(fontSize: 15.0),
                                  ),
                                )),
                          ),
                          new Expanded(
                              flex: 0,
                              child: new Padding(
                                padding: new EdgeInsets.only(right: 10.0),
                                child: new Icon(Icons.content_copy),
                              ))
                        ],
                      )
                    ],
                  ),
                ),
              ),
            );
          }),
      floatingActionButton: new FloatingActionButton(
        child: new Icon(Icons.add),
        onPressed: () {
          reset();
          Navigator
              .of(context)
              .push(new MaterialPageRoute(builder: (BuildContext context) {
            return new _OneClickTab.OneClick();
          }));
        },
      ),
    );
  }
}
