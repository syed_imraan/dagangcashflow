import 'dart:async';
import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:my_mini_web/library/GlobalVariable.dart' as globals;

final String url = "http://dagang.myminiweb.com/public/api/get-record/" +
    globals.userid.toString();
List listData;
var Data = [];
var value = "0.00";
var expenses = "0.00";
var profit = "0.00";
var bankin = "0.00";
var result = "0.00";

var Yearlyvalue = "0.00";
var Yearlyexpenses = "0.00";
var Yearlyprofit = "0.00";
var Yearlybankin = "0.00";
var Yearlyresult = "0.00";

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

// Text Box Read Only
class AlwaysDisabledFocusNode extends FocusNode {
  @override
  bool get hasFocus => false;
}

class _MyHomePageState extends State<MyHomePage> {
  List data = new List();

  @override
  void initState() {
    super.initState();
    //this.getData();
    this.getData().then((result) {
      print(result);
      setState(() {
        var testData = jsonDecode(result);
        if (testData['cash_in'] != Null) value = testData['cash_in'];
        if (testData['cash_out'] != Null) expenses = testData['cash_out'];
        if (testData['profit'] != Null) profit = testData['profit'];
        if (testData['bank_in'] != Null) bankin = testData['bank_in'];
        if (testData['cash_in_year'] != Null)
          Yearlyvalue = testData['cash_in_year'];
        if (testData['cash_out_year'] != Null)
          Yearlyexpenses = testData['cash_out_year'];
        if (testData['profit_year'] != Null)
          Yearlyprofit = testData['profit_year'];
        if (testData['bank_in_year'] != Null)
          Yearlybankin = testData['bank_in_year'];
        data = testData['all_copy'];
        //print(data);
      });

      return 'next';
    });
  }

  Future<String> getData() async {
    String status = "";
    try {
      var connectivityResult = await (new Connectivity().checkConnectivity());
      if (connectivityResult == ConnectivityResult.mobile ||
          connectivityResult == ConnectivityResult.wifi) {
        // I am connected to a mobile network. or I am connected to a wifi network.
        print("connected");

        var response = await http
            .get(Uri.encodeFull(url), headers: {"Accept": "application/json"});

        return response.body;
      } else {
        print("Not connected");
      }

      print(status);
      return status;
    } catch (e) {
      status =
          "Please Contact or Report to system Administrator"; //e.toString();
    }
    return "Success";
  }

  Widget _buildChild(int type) {
    if (type == 1) {
      final txtValue = new Card(
        child: new Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            SizedBox(height: 18.00),
            new Row(
              children: <Widget>[
                SizedBox(width: 15.00),
                new Text('TOTAL SALES',
                    style: new TextStyle(
                      fontSize: 15.0,
                      fontWeight: FontWeight.bold,
                    )),
              ],
            ),
            SizedBox(height: 10.00),
            new Row(
              children: <Widget>[
                SizedBox(width: 15.00),
                new Expanded(
                  flex: 0,
                  child: new Text('RM',
                      style: new TextStyle(
                        fontSize: 15.0,
                        fontWeight: FontWeight.bold,
                      )),
                ),
                new Expanded(
                  child: new Text(
                    //formatDecimal(value),
                    value,
                    textAlign: TextAlign.right,
                    style: TextStyle(
                      fontSize: 15.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                SizedBox(
                  width: 15.00,
                )
              ],
            ),
            SizedBox(height: 18.00),
          ],
        ),
        shape: new RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
            side: new BorderSide(
                style: BorderStyle.solid, color: Colors.grey, width: 2.00)),
      );

      return txtValue;
    } else if (type == 2) {
      final txtExpense = new Card(
        child: new Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            SizedBox(height: 18.00),
            new Row(
              children: <Widget>[
                SizedBox(width: 15.00),
                new Text('TOTAL EXPENSES',
                    style: new TextStyle(
                      fontSize: 15.0,
                      fontWeight: FontWeight.bold,
                    )),
              ],
            ),
            SizedBox(height: 10.00),
            new Row(
              children: <Widget>[
                SizedBox(width: 15.00),
                new Expanded(
                  flex: 0,
                  child: new Text('RM',
                      style: new TextStyle(
                        fontSize: 15.0,
                        fontWeight: FontWeight.bold,
                      )),
                ),
                new Expanded(
                  child: new Text(
                    //formatDecimal(value),
                    expenses,
                    textAlign: TextAlign.right,
                    style: TextStyle(
                      fontSize: 15.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                SizedBox(
                  width: 15.00,
                )
              ],
            ),
            SizedBox(height: 18.00),
          ],
        ),
        shape: new RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
            side: new BorderSide(
                style: BorderStyle.solid, color: Colors.grey, width: 2.00)),
      );

      return txtExpense;
    } else if (type == 3) {
      final txtProfit = new Card(
        child: new Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            SizedBox(height: 18.00),
            new Row(
              children: <Widget>[
                SizedBox(width: 15.00),
                new Text('TOTAL PROFIT',
                    style: new TextStyle(
                      fontSize: 15.0,
                      fontWeight: FontWeight.bold,
                    )),
              ],
            ),
            SizedBox(height: 10.00),
            new Row(
              children: <Widget>[
                SizedBox(width: 15.00),
                new Expanded(
                  flex: 0,
                  child: new Text('RM',
                      style: new TextStyle(
                        fontSize: 15.0,
                        fontWeight: FontWeight.bold,
                      )),
                ),
                new Expanded(
                  child: new Text(
                    //formatDecimal(value),
                    profit,
                    textAlign: TextAlign.right,
                    style: TextStyle(
                      fontSize: 15.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                SizedBox(
                  width: 15.00,
                )
              ],
            ),
            SizedBox(height: 18.00),
          ],
        ),
        shape: new RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
            side: new BorderSide(
                style: BorderStyle.solid, color: Colors.grey, width: 2.00)),
      );

      return txtProfit;
    }
    if (type == 4) {
      final txtYearBankIn = new Card(
        child: new Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            SizedBox(height: 18.00),
            new Row(
              children: <Widget>[
                SizedBox(width: 15.00),
                new Text('TOTAL BANK IN',
                    style: new TextStyle(
                      fontSize: 15.0,
                      fontWeight: FontWeight.bold,
                    )),
              ],
            ),
            SizedBox(height: 10.00),
            new Row(
              children: <Widget>[
                SizedBox(width: 15.00),
                new Expanded(
                  flex: 0,
                  child: new Text('RM',
                      style: new TextStyle(
                        fontSize: 15.0,
                        fontWeight: FontWeight.bold,
                      )),
                ),
                new Expanded(
                  child: new Text(
                    //formatDecimal(value),
                    bankin,
                    textAlign: TextAlign.right,
                    style: TextStyle(
                      fontSize: 15.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                SizedBox(
                  width: 15.00,
                )
              ],
            ),
            SizedBox(height: 18.00),
          ],
        ),
        shape: new RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
            side: new BorderSide(
                style: BorderStyle.solid, color: Colors.grey, width: 2.00)),
      );

      return txtYearBankIn;
    }

    if (type == 5) {
      final txtYearValue = new Card(
        child: new Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            SizedBox(height: 18.00),
            new Row(
              children: <Widget>[
                SizedBox(width: 15.00),
                new Text('TOTAL SALES',
                    style: new TextStyle(
                      fontSize: 15.0,
                      fontWeight: FontWeight.bold,
                    )),
              ],
            ),
            SizedBox(height: 10.00),
            new Row(
              children: <Widget>[
                SizedBox(width: 15.00),
                new Expanded(
                  flex: 0,
                  child: new Text('RM',
                      style: new TextStyle(
                        fontSize: 15.0,
                        fontWeight: FontWeight.bold,
                      )),
                ),
                new Expanded(
                  child: new Text(
                    //formatDecimal(value),
                    Yearlyvalue,
                    textAlign: TextAlign.right,
                    style: TextStyle(
                      fontSize: 15.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                SizedBox(
                  width: 15.00,
                )
              ],
            ),
            SizedBox(height: 18.00),
          ],
        ),
        shape: new RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
            side: new BorderSide(
                style: BorderStyle.solid, color: Colors.grey, width: 2.00)),
      );

      return txtYearValue;
    } else if (type == 6) {
      final txtYearExpense = new Card(
        child: new Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            SizedBox(height: 18.00),
            new Row(
              children: <Widget>[
                SizedBox(width: 15.00),
                new Text('TOTAL EXPENSES',
                    style: new TextStyle(
                      fontSize: 15.0,
                      fontWeight: FontWeight.bold,
                    )),
              ],
            ),
            SizedBox(height: 10.00),
            new Row(
              children: <Widget>[
                SizedBox(width: 15.00),
                new Expanded(
                  flex: 0,
                  child: new Text('RM',
                      style: new TextStyle(
                        fontSize: 15.0,
                        fontWeight: FontWeight.bold,
                      )),
                ),
                new Expanded(
                  child: new Text(
                    //formatDecimal(value),
                    Yearlyexpenses,
                    textAlign: TextAlign.right,
                    style: TextStyle(
                      fontSize: 15.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                SizedBox(
                  width: 15.00,
                )
              ],
            ),
            SizedBox(height: 18.00),
          ],
        ),
        shape: new RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
            side: new BorderSide(
                style: BorderStyle.solid, color: Colors.grey, width: 2.00)),
      );

      return txtYearExpense;
    } else if (type == 7) {
      final txtYearProfit = new Card(
        child: new Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            SizedBox(height: 18.00),
            new Row(
              children: <Widget>[
                SizedBox(width: 15.00),
                new Text('TOTAL PROFIT',
                    style: new TextStyle(
                      fontSize: 15.0,
                      fontWeight: FontWeight.bold,
                    )),
              ],
            ),
            SizedBox(height: 10.00),
            new Row(
              children: <Widget>[
                SizedBox(width: 15.00),
                new Expanded(
                  flex: 0,
                  child: new Text('RM',
                      style: new TextStyle(
                        fontSize: 15.0,
                        fontWeight: FontWeight.bold,
                      )),
                ),
                new Expanded(
                  child: new Text(
                    //formatDecimal(value),
                    Yearlyprofit,
                    textAlign: TextAlign.right,
                    style: TextStyle(
                      fontSize: 15.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                SizedBox(
                  width: 15.00,
                )
              ],
            ),
            SizedBox(height: 18.00),
          ],
        ),
        shape: new RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
            side: new BorderSide(
                style: BorderStyle.solid, color: Colors.grey, width: 2.00)),
      );

      return txtYearProfit;
    }

    final txtYearBankIn = new Card(
      child: new Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          SizedBox(height: 18.00),
          new Row(
            children: <Widget>[
              SizedBox(width: 15.00),
              new Text('TOTAL BANK IN',
                  style: new TextStyle(
                    fontSize: 15.0,
                    fontWeight: FontWeight.bold,
                  )),
            ],
          ),
          SizedBox(height: 10.00),
          new Row(
            children: <Widget>[
              SizedBox(width: 15.00),
              new Expanded(
                flex: 0,
                child: new Text('RM',
                    style: new TextStyle(
                      fontSize: 15.0,
                      fontWeight: FontWeight.bold,
                    )),
              ),
              new Expanded(
                child: new Text(
                  //formatDecimal(value),
                  Yearlybankin,
                  textAlign: TextAlign.right,
                  style: TextStyle(
                    fontSize: 15.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              SizedBox(
                width: 15.00,
              )
            ],
          ),
          SizedBox(height: 18.00),
        ],
      ),
      shape: new RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
          side: new BorderSide(
              style: BorderStyle.solid, color: Colors.grey, width: 2.00)),
    );

    return txtYearBankIn;
  }

  Widget _binaListTengok() {
    final tengok = new ListView.builder(
        itemCount: data == null ? 0 : data.length,
        shrinkWrap: true,
        itemBuilder: (BuildContext context, int index) {
//final item = data[index]['id'].toString();

          return new Card(
//I am the clickable child
              child: new Row(
            children: <Widget>[
              new Expanded(
                flex: 1,
                child: new Padding(
                    padding: new EdgeInsets.all(5.0),
                    child: new Container(
                      child: new Text(
//'test',
                        "No " +
                            index.toString() +
                            ".    " +
                            data[index]["description"].toString(),
                        style: new TextStyle(fontSize: 15.0),
                      ),
                    )),
              ),
            ],
          ));
        });

    return tengok;
  }

  Widget _binaTkde() {
    return new Text('No data!');
  }

  String formatDecimal(double number) {
    if (number > -1000 && number < 1000) return number.toString();

    final String digits = number.abs().toString();
    final StringBuffer result = new StringBuffer(number < 0 ? '-' : '');
    final int maxDigitIndex = digits.length - 1;
    for (int i = 0; i <= maxDigitIndex; i += 1) {
      result.write(digits[i]);
      if (i < maxDigitIndex && (maxDigitIndex - i) % 5 == 0) result.write(',');
    }
    return result.toString();
  }

  @override
  Widget build(BuildContext context) {
    final key = new GlobalKey<ScaffoldState>();
    return new MaterialApp(
        home: new DefaultTabController(
            length: 2,
            child: new Scaffold(
              appBar: new AppBar(
                flexibleSpace: new Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    new TabBar(
                      tabs: [
                        new Tab(
                          text: "This Month",
                        ),
                        new Tab(
                          text: "Yearly",
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              key: key,
              body: TabBarView(children: <Widget>[
                new ListView(
                  shrinkWrap: true,
                  padding: EdgeInsets.only(left: 24.0, right: 24.0),
                  children: <Widget>[
                    _buildChild(1),
                    _buildChild(2),
                    _buildChild(3),
                    _buildChild(4),
                    new Card(
                        shape: new RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0),
                            side: new BorderSide(
                                style: BorderStyle.solid,
                                color: Colors.grey,
                                width: 2.00)),
                        child: new Container(
                            //height: 150.0,
                            child: new Column(
                          children: <Widget>[
                            new Padding(
                              padding: new EdgeInsets.all(2.0),
                              child: new Text(
                                'TO DO LIST',
                                style: new TextStyle(
                                    fontSize: 15.0,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            data.length == 0 ? _binaTkde() : _binaListTengok(),
                          ],
                        ))),
                  ],
                ),
                new ListView(
                  shrinkWrap: true,
                  padding: EdgeInsets.only(left: 24.0, right: 24.0),
                  children: <Widget>[
                    _buildChild(5),
                    _buildChild(6),
                    _buildChild(7),
                    _buildChild(8),
                    new Card(
                        shape: new RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0),
                            side: new BorderSide(
                                style: BorderStyle.solid,
                                color: Colors.grey,
                                width: 2.00)),
                        child: new Container(
                            //height: 150.0,
                            child: new Column(
                          children: <Widget>[
                            new Padding(
                              padding: new EdgeInsets.all(2.0),
                              child: new Text(
                                'TO DO LIST',
                                style: new TextStyle(
                                    fontSize: 15.0,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            data.length == 0 ? _binaTkde() : _binaListTengok(),
                          ],
                        ))),
                  ],
                ),
              ]),
            )));
  }

//  @override
//  Widget build(BuildContext context) {
//    return new CustomScrollView(
//      slivers: <Widget>[
  ///*        const SliverAppBar(
//          pinned: true,
//          expandedHeight: 250.0,
//          flexibleSpace: const FlexibleSpaceBar(
//            title: const Text('Demo'),
//          ),
//        ),*/
//        new SliverGrid(
//          gridDelegate: new SliverGridDelegateWithMaxCrossAxisExtent(
//            maxCrossAxisExtent: 200.0,
//            mainAxisSpacing: 10.0,
//            crossAxisSpacing: 10.0,
//            childAspectRatio: 4.0,
//          ),
//          delegate: new SliverChildBuilderDelegate(
//                (BuildContext context, int index) {
//              return new Container(
//                alignment: Alignment.center,
//                color: Colors.teal[100 * (index % 9)],
//                child: new Text('grid item $index'),
//              );
//            },
//            childCount: 20,
//          ),
//        ),
//        new SliverFixedExtentList(
//          itemExtent: 50.0,
//          delegate: new SliverChildBuilderDelegate(
//                (BuildContext context, int index) {
//              return new Container(
//                alignment: Alignment.center,
//                color: Colors.lightBlue[100 * (index % 9)],
//                child: new Text('list item $index'),
//              );
//            },
//          ),
//        ),
//      ],
//    );

//    final key = new GlobalKey<ScaffoldState>();
//    return new Scaffold(
//        key: key,
//        body: new Column(
//          mainAxisSize: MainAxisSize.min,
//          mainAxisAlignment: MainAxisAlignment.spaceAround,
//          children: <Widget>[
//            _buildChild(1),
//            _buildChild(2),
//            _buildChild(3),
//            _buildChild(4),
//            new Expanded(
//              child: new Card(
//                shape: new RoundedRectangleBorder(
//                    borderRadius: BorderRadius.circular(15.0),
//                    side: new BorderSide(
//                        style: BorderStyle.solid,
//                        color: Colors.grey,
//                        width: 2.00)),
//                child: new ListView.builder(
//                    itemCount: data == null ? 0 : data.length,
//                    shrinkWrap: true,
//                    itemBuilder: (BuildContext context, int index) {
////final item = data[index]['id'].toString();
//                      print(data);
//                      print('wpolasdasd');
//                      return new Card(
////I am the clickable child
//                          child: new Row(
//                        children: <Widget>[
//                          new Expanded(
//                            flex: 1,
//                            child: new Padding(
//                                padding: new EdgeInsets.all(5.0),
//                                child: new Container(
//                                  child: new Text(
////'test',
//                                    data[index]["description"].toString(),
//                                    style: new TextStyle(fontSize: 15.0),
//                                  ),
//                                )),
//                          ),
//                        ],
//                      ));
//                    }),
//              ),
//            )
//          ],
//        ));
// }
}
