import 'package:flutter/material.dart';
import 'package:my_mini_web/library/GlobalVariable.dart' as globals;



final titleController = TextEditingController();
final amountController = TextEditingController();
final contactusController = TextEditingController();
final dateController = TextEditingController();

// Text Box Read Only
class AlwaysDisabledFocusNode extends FocusNode {
  @override
  bool get hasFocus => false;
}

class Details extends StatefulWidget {
  @override
  _DetailsPageState createState() => new _DetailsPageState();
}

class _DetailsPageState extends State<Details> {
  void reset() {
    titleController.text = globals.title;
    amountController.text = globals.amount.toString();
    contactusController.text = globals.contactus;
    dateController.text = globals.date;
  }

  @override
  void initState() {
    super.initState();
    this.reset();
    print("title:" +
        globals.title +
        "amount:" +
        globals.amount.toString() +
        "contactus:" +
        globals.contactus +
        "date:" +
        globals.date +
        "image:" +
        globals.image +
        "type:" +
        globals.type);
  }

  @override
  Widget build(BuildContext context) {
    final _date = DateTime.parse(globals.date);

    return Scaffold(
      //App Bar
      appBar: new AppBar(
        backgroundColor: new Color(0xFFFFB300),
        title: new Text(
          'Details',
          style: new TextStyle(
            fontSize:
                Theme.of(context).platform == TargetPlatform.iOS ? 17.0 : 20.0,
            color: Colors.black,
          ),
        ),
        elevation: Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
      ),
      backgroundColor: Colors.white,
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left: 10.0, right: 10.0),
          children: <Widget>[
            new Container(
              padding: new EdgeInsets.only(top: 10.0),
              child: Image.network(
                  'http://dagang.myminiweb.com/public/uploads/' +
                      globals.image),
              width: 350.0,
              height: 350.0,
            ),
            new TextFormField(
              controller: titleController,
              focusNode: new AlwaysDisabledFocusNode(),
              style: new TextStyle(fontSize: 15.0, color: Colors.black),
              maxLines: 2,
              decoration: InputDecoration(
                  labelText: 'Title :  ',
                  labelStyle: new TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 20.0)),
            ),
            new TextFormField(
              controller: amountController,
              focusNode: new AlwaysDisabledFocusNode(),
              style: new TextStyle(fontSize: 15.0, color: Colors.black),
              decoration: InputDecoration(
                  labelText: 'RM :  ',
                  labelStyle: new TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 20.0)),
            ),
            new TextFormField(
              controller: contactusController,
              focusNode: new AlwaysDisabledFocusNode(),
              style: new TextStyle(fontSize: 15.0, color: Colors.black),
              decoration: InputDecoration(
                  labelText: 'Contact :  ',
                  labelStyle: new TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 20.0)),
            ),
            new TextFormField(
              controller: dateController,
              focusNode: new AlwaysDisabledFocusNode(),
              style: new TextStyle(fontSize: 15.0, color: Colors.black),
              decoration: InputDecoration(
                  labelText: 'Date :  ',
                  labelStyle: new TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 20.0)),
            )
          ],
        ),
      ),
    );
  }
}
