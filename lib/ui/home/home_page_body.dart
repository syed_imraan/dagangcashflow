import 'package:flutter/material.dart';
import 'package:my_mini_web/model/planets_.dart';

import 'planet_row.dart';


class HomePageBody extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return new Expanded(
      child: new Container(
        /*color: new Color(0xFF736AB7),*/
        color: new Color(0xFF212121),
        child: new CustomScrollView(
          scrollDirection: Axis.vertical,
          slivers: <Widget>[
            new SliverPadding(
              padding: const EdgeInsets.symmetric(vertical: 24.0),
              sliver: new SliverFixedExtentList(
                itemExtent: 152.0,
                delegate: new SliverChildBuilderDelegate(
                      (context, index) => new PlanetRow(planetts[index]),
                  childCount: planetts.length,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}