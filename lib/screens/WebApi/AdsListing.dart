import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:html/parser.dart'  show parse;
import 'package:html/dom.dart' as domDart;

import 'package:flutter_html_view/flutter_html_view.dart';

var testData = [];
var lolo = "";

void main() async {

  runApp(new MaterialApp(
    title: "GRID dan HERO",
    home: new Home(),
  ));
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => new _HomeState();
}

class _HomeState extends State<Home> {

  List<Container> container = new List();

  var gData;

  static getData() async {
    print("tesT");
    var response = await http.get(
        Uri.encodeFull("http://192.168.0.145/lara_test/public/api/get-all-record"),
        headers: {
          "Accept": "application/json"
        }
    );
    var listdata = json.decode(response.body);

    for (var data in listdata) {
      testData.add({
        "title": data["title"],
        "amount": data["amount"],
        "date" : data["date"]
      });
    }

    return "Success";
  }
/*

  listing() {
    print("listing");
    for (var data in testData) {
        final String gambar = data["picture"];
      //print(gambar);

      container.add(new Container(
          padding: new EdgeInsets.all(10.0),
          child: new Card(
              child: new Column(
                children: <Widget>[
                  new Hero(
                    tag: data["title"],
                    child: new Material(
                      child: new InkWell(
                        onTap: () =>
                            Navigator.of(context).push(new MaterialPageRoute(
                              builder: (BuildContext context) =>
                              new Detail(
                                nama: data["title"],
                                gambar: gambar,
                                description: data["description"],
                              ),
                            )),
                        child: new Image.network(
                          "https://www.myminiweb.com/pic/x/cache/medium/" + gambar,
                          //"img/captain.jpg",)
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                  ),
                  new Padding(
                    padding: new EdgeInsets.all(10.0),
                  ),
                  new Text(
                    data["title"],
                    style: new TextStyle(fontSize: 12.0),
                  )
                ],
              )
          )
      ));
    }
  }
*/

  @override
  void initState() {
    getData();
    //isting();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return new Scaffold(
/*      appBar: new AppBar(
        title: new Text(
          "MyMiniWeb",
          style: new TextStyle(color: Colors.white),
        ),
      ),*/
      body: new GridView.count(
        crossAxisCount: 2,
        children: container,
      ),
    );
  }
}

class Detail extends StatelessWidget {
  Detail({this.nama, this.gambar, this.description});
  final String nama;
  final String gambar;
  final String description;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new ListView(
        children: <Widget>[
          new Container(
              height: 240.0,
              child: new Hero(
                tag: nama,
                child: new Material(
                  child: new InkWell(
                    child: new Image.network(
                      "https://www.myminiweb.com/pic/x/cache/medium/" + gambar,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              )),
          new BagianNama(
            nama: nama,
          ),
          new BagianIcon(),
          new Details(
            description: description
          ),//description :description),
        ],
      ),
    );
  }
}

class BagianNama extends StatelessWidget {
  BagianNama({this.nama});
  final String nama;

  @override
  Widget build(BuildContext context) {
    return new Container(
      padding: new EdgeInsets.all(10.0),
      child: new Row(
        children: <Widget>[
          new Expanded(
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Text(
                  nama,
                  style: new TextStyle(fontSize: 20.0, color: Colors.blue),
                ),
                new Text(
                  "$nama\@gmail.com",
                  style: new TextStyle(fontSize: 17.0, color: Colors.grey),
                ),
              ],
            ),
          ),
          new Row(
            children: <Widget>[
              new Icon(
                Icons.star,
                size: 30.0,
                color: Colors.red,
              ),
              new Text(
                "12",
                style: new TextStyle(fontSize: 18.0),
              )
            ],
          )
        ],
      ),
    );
  }
}

class BagianIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Container(
      padding: new EdgeInsets.all(10.0),
      child: new Row(
        children: <Widget>[
          new Iconteks(
            icon: Icons.call,
            teks: "Call",
          ),
          new Iconteks(
            icon: Icons.message,
            teks: "Message",
          ),
          new Iconteks(
            icon: Icons.photo,
            teks: "Photo",
          ),
        ],
      ),
    );
  }
}

class Iconteks extends StatelessWidget {
  Iconteks({this.icon, this.teks});
  final IconData icon;
  final String teks;
  @override
  Widget build(BuildContext context) {
    return new Expanded(
      child: new Column(
        children: <Widget>[
          new Icon(
            icon,
            size: 50.0,
            color: Colors.blue,
          ),
          new Text(
            teks,
            style: new TextStyle(fontSize: 18.0, color: Colors.blue),
          )
        ],
      ),
    );
  }
}

class Details extends StatelessWidget {
  Details({this.description});
  final String description;

  @override
  Widget build(BuildContext context) {
    return new Container(
      padding: new EdgeInsets.all(10.0),
      child: new Card(
        child: new Padding(
          padding: const EdgeInsets.all(10.0),
            child: HtmlView(data: description,
//          child: new Text(
//            description,//"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
//            style: new TextStyle(fontSize: 18.0),
//            textAlign: TextAlign.justify,
          ),
        ),
      ),
    );
  }
}
