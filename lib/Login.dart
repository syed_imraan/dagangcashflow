/*
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/foundation.dart';

// ignore: camel_case_types
class login extends StatelessWidget{
    const login ({
      Key key,
      @required this.onSubmit,
  }) : super(key:key);

    final VoidCallback onSubmit;
    static final TextEditingController _user = new TextEditingController();
    static final TextEditingController _pass = new TextEditingController();

    String get username => _user.text;
    String get password => _pass.text;

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        new TextField(controller: _user, decoration: new InputDecoration(hintText: 'Please Enter User name'),),
        new TextField(controller: _pass, decoration: new InputDecoration(hintText: 'Please Enter Passwrod'), obscureText: true,),
        new RaisedButton(child: new Text('Submit'), onPressed: onSubmit)
      ],
    );
  }
}*/
import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:my_mini_web/library/GlobalVariable.dart' as globals;
import 'package:my_mini_web/main.dart';

final String url = "https://oag.myminiweb.com/api/dagang_login";
String status = "";
final usernameController = TextEditingController();
final passwordController = TextEditingController();

class LoginPage extends StatefulWidget {
  static String tag = 'login-page';

  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  postData(context) async {
    status = "";
    _onLoading();
    try {
      var connectivityResult = await (new Connectivity().checkConnectivity());
      if (connectivityResult == ConnectivityResult.mobile ||
          connectivityResult == ConnectivityResult.wifi) {
        // I am connected to a mobile network. or I am connected to a wifi network.
        status = "connected";

        var uri = Uri.parse(url);
        var request = new http.MultipartRequest("POST", uri);
        request.fields['username'] = 'oag1'; //usernameController.text;
        request.fields['password'] = '123456'; //passwordController.text;
        request.send().then((streamedResponse) {
          return http.Response.fromStream(streamedResponse);
        }).then((response) {
          var data = jsonDecode(response.body);
          if (data != null) {
            if (data["status_code"] == 200) {
              print(data["message"]);
              print(data["user"]["id"].toString());
              //_showAlert(context, Data["message"]);
              globals.userid = int.parse(data["user"]["id"]);
              globals.username = data["user"]["username"];
              globals.isLogin = true;
              Navigator.pop(context); //Close loading dialog
              Navigator.of(context).pushNamed(Tabs.tag);
            } else {
              print(data["message"]);
              Navigator.pop(context); //Close loading dialog
              _showAlert(context, data["message"]);
            }
          }
        });
      } else {
        status = "Not connected";
      }
      print(status);
      return status;
    } catch (e) {
      Navigator.pop(context); //Close loading dialog
      status =
          "Please Contact or Report to system Administrator"; //e.toString();
    }

    return "Success";
  }


  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      child: new Dialog(
        child: new Padding(
          padding: EdgeInsets.all(20.0),
          child: new Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              new CircularProgressIndicator(),
              new Expanded(
                child: new Text("Loading..", textAlign: TextAlign.center),
              )
            ],
          ),
        ),
      ),
    );
    //new Timer(new Duration(seconds: 3), () => Navigator.pop(context));
  }

  void _showAlert(context, String msj) {
    AlertDialog dialog = new AlertDialog(
      content: new Text(msj),
      actions: <Widget>[
        new FlatButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: new Text('Ok'),
        )
      ],
    );

    showDialog(context: context, child: dialog);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //App Bar
/*      appBar: new AppBar(
        title: new Text(
          'Login',
          style: new TextStyle(
            fontSize: Theme.of(context).platform == TargetPlatform.iOS ? 17.0 : 20.0,
          ),
        ),
        elevation: Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
      ),*/
      backgroundColor: Colors.yellow.shade600,
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left: 24.0, right: 24.0),
          children: <Widget>[
            new Hero(
              tag: 'hero',
              child: CircleAvatar(
                backgroundColor: Colors.transparent,
                radius: 48.0,
                child: Image.asset('assets/img/dagang_logo.png'),
              ),
            ),
            SizedBox(height: 48.0),
            new TextFormField(
              controller: usernameController,
              keyboardType: TextInputType.text,
              autofocus: false,
              decoration: InputDecoration(
                filled: true,
                fillColor: Colors.yellow.shade100,
                hintText: 'username',
                contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(32.0)),
              ),
            ),
            SizedBox(height: 8.0),
            new TextFormField(
              obscureText: true,
              controller: passwordController,
              keyboardType: TextInputType.text,
              autofocus: false,
              decoration: InputDecoration(
                filled: true,
                fillColor: Colors.yellow.shade100,
                hintText: 'password',
                contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(32.0)),
              ),
            ),
            SizedBox(height: 24.0),
            new Padding(
              padding: EdgeInsets.symmetric(vertical: 16.0),
              child: Material(
                borderRadius: BorderRadius.circular(30.0),
                shadowColor: Colors.black,
                elevation: 5.0,
                child: MaterialButton(
                  minWidth: 200.0,
                  height: 42.0,
                  onPressed: () {
                    postData(context);
                  },
                  //postData,
                  color: Colors.black,
                  child: Text('Submit', style: TextStyle(color: Colors.white)),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
